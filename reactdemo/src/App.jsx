import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Auth from './components/Auth';
import Login from './container/Login';
import WelcomeDraw from './container/WelcomeDraw';

class App extends React.Component {
    render() {
        return (
            <div>
                <Auth></Auth>
                <Switch>
                    <Route path='/' exact component={Login}></Route>
                    <Route path='/login' component={Login}></Route>
                    <Route path='/WelcomeDraw' component={WelcomeDraw}></Route>
                </Switch>
            </div>
        )
    }
}


export default App;