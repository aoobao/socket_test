import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import reducer from './reducers/index';
import App from './App';

import api from './api/socket';

import {} from './api/util';
import './css/main.css';
//import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducer, compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

function render() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    );
}

//建立socket连接
let socket = api.socket;

socket.on('connect', () => {
    console.log('服务器连接成功');
    var name = api.getItem('name');
    if(name){
        api.login(name);
    }
    ReactDOM.render(render(), document.getElementById('root'));
});

socket.on("disconnect", () => {
    console.log("disconnect");
    //跳转连接失败页面 TODO
});




//registerServiceWorker();
