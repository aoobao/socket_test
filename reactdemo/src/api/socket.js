import io from 'socket.io-client';
import config from '../config';
import msg from './connectMsg';

var socket = window.socket = io(config.Url);


export default {
    socket: window.socket,
    //登录用户名
    login(userName, fn) {
        console.log('设置用户名:' + userName);
        this.socket.emit(msg.USER_LOGIN, userName, fn);
    },
    logOut(fn) {
        console.log('用户登出');
        socket.emit(msg.USER_LOGOUT, fn);
    },
    //获取当然房间列表.
    getRoomList(fn) {
        console.log('获取房间列表');
        socket.emit(msg.GET_ROOM_LIST, fn);
    },

    addRoom(roomName, fn) {
        console.log('请求创建房间');
        socket.emit(msg.ADD_ROOM, roomName, fn);
    },
    removeRoom(key, fn) {
        socket.emit(msg.REMOVE_ROOM, key, fn);
    },
    getRoomInfo(key, fn) {
        socket.emit(msg.GET_ROOM_INFO, key, fn);
    },
    addDrawInfo(drawInfo, fn) {
        this.socket.emit(msg.ADD_DRAW_INFO, drawInfo, fn);
    },
    //离开房间.
    leaveRoom(fn) {
        console.log('离开房间');
        socket.emit(msg.LEAVE_ROOM, fn);
    },
    getItem: function (value) {
        if (window.localStorage) {
            return window.localStorage.getItem(value);
        } else {
            return this.getCookie(value);
        }
    },
    //设置 值
    setItem: function (name, value) {
        if (window.localStorage) {
            return window.localStorage.setItem(name, value);
        } else {
            return this.setCookie(name, value);
        }

    },
    getCookie: function (keyName) {
        var cookieTrim = this.trim(document.cookie, "g");
        var reg = new RegExp("(^|;)" + keyName + "=([^;]*)(;|$)");
        var arr = cookieTrim.match(reg)
        if (arr)
            return unescape(arr[2]);
        else
            return null;
    },

    //设置Cookie
    setCookie: function (keyName, keyValue, expireMins = 1440) {
        var date = new Date();
        date.setTime(date.getTime() + expireMins * 60 * 1000);
        document.cookie = keyName + " = " + escape(keyValue) + ((expireMins == null) ? "" : "; expire = " + date.toGMTString());
    },

}


// function newGuid() {
//     var guid = "";
//     for (var i = 1; i <= 32; i++) {
//         var n = Math.floor(Math.random() * 16.0).toString(16);
//         guid += n;
//         if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
//             guid += "-";
//     }
//     return guid;
// }

// function success(data, msg = '成功', number = 1) {
//     let obj = {
//         code: number,
//         msg
//     };
//     if (data != undefined) {
//         obj.data = data;
//     }
//     return obj;
// }

// function err(data, msg = '失败', number = 0) {
//     let obj = {
//         code: number,
//         msg
//     };
//     if (data != undefined) {
//         obj.data = data;
//     }
//     return obj;
// }

// const arr = [{
//     roomName: 'feng的房间1',
//     createUserKey: 'admin',
//     key: 1,
//     imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
//     personList: [],
//     createTime: new Date()
// }, {
//     roomName: 'feng的房间2',
//     createUserKey: 'admin',
//     key: 2,
//     imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
//     personList: [{ userName: 'aoobao', userKey: '1' }, { userName: 'wa', userKey: '4' }],
//     createTime: new Date()
// }, {
//     roomName: 'feng的房间3',
//     createUserKey: 'admin',
//     key: 3,
//     imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
//     personList: [{ userName: 'aoobao', userKey: '1' }, { userName: 'feng', userKey: '2' },
//     { userName: 'cl', userKey: '3' }, { userName: 'wa', userKey: '4' }],
//     createTime: new Date()
// }];

// var key = arr.length + 1;