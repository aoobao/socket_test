var msg = {
    //连接成功,服务器返回
    CONNECT_SUCCESS: 'connect_success',
    //用户登录请求
    USER_LOGIN: 'user_login',
    //用户登出请求
    USER_LOGOUT: 'user_logout',
    //服务器返回 登录是否成功
    LOGIN_SUCCESS: 'login_success',
    //服务器返回 人员登录状态.
    USER_JOIN: 'user_join',
    //获取房间列表
    GET_ROOM_LIST: 'get_room_list',
    //添加新房间
    ADD_ROOM: 'add_room',
    //加入房间.
    JOIN_ROOM:'join_room',
    //离开房间
    LEAVE_ROOM:'leave_room', 
    //删除房间
    REMOVE_ROOM: 'remove_room',
    //获取房间详情
    GET_ROOM_INFO: 'get_room_info',
    //房间信息变动(服务器主动推送.)
    GET_ROOM_INFO_CHANGE:'room info change',
    //添加画板内容
    ADD_DRAW_INFO:'add_draw_info',
    //test
    TEST:'add user',
}


export default msg;