import React from 'react'
import { connect } from 'react-redux';

import {  NavBar, Icon, Modal, Toast } from 'antd-mobile';

import DrawRoom from './DrawRoom';
import RoomList from './RoomList';
import Footer from '../components/Footer';

import { logout } from '../reducers/modules/personInfo';

import { createRoom, getRoomInfo, switchSelectTab, removeRoom, setRoomInfo, leaveRoom } from '../reducers/modules/drawInfo';

@connect(state => {
    var obj = {
        ...state.personInfo,
        drawInfo: state.drawInfo.drawInfo
    };
    return obj;
}, { logout, createRoom, getRoomInfo, switchSelectTab, removeRoom, setRoomInfo, leaveRoom })
class WelcomeDraw extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //selectedTab: 'blueTab',
            hidden: false,
            fullScreen: true,
            tabList: [{
                title: '房间列表',
                key: 'blueTab',
                iconUrl: 'https://zos.alipayobjects.com/rmsportal/sifuoDUQdAFKAVcFGROC.svg',
                selectedIconUrl: 'https://zos.alipayobjects.com/rmsportal/iSrlOTqrKddqbOmlvUfq.svg',
                body: 'RoomList'
            }, {
                title: '房间详情',
                key: 'yellowTab',
                iconUrl: 'https://gw.alipayobjects.com/zos/rmsportal/BTSsmHkPsQSPTktcXyTV.svg',
                selectedIconUrl: 'https://gw.alipayobjects.com/zos/rmsportal/ekLecvKBnRazVLXbWOnE.svg',
                body: 'DrawRoom'
            }],
            selectedTab: 'blueTab'
        };
        this.logout = this.logout.bind(this);
        this.leaveRoom = this.leaveRoom.bind(this);
        this.promptRoom = this.promptRoom.bind(this);
        this.removeRoom = this.removeRoom.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.changeTab = this.changeTab.bind(this);
    }
    componentDidMount() {
        // console.log('welcomdraw', this.props);

    }
    logout() {
        this.props.logout(null);
        this.props.setRoomInfo(null);
        this.props.history.replace('/login');
    }
    leaveRoom() {
        // console.log(this.props);
        this.props.leaveRoom();
        this.changeTab('blueTab');
    }
    createRoom(value) {

        if (!value) {
            Toast.info('请输入房间名称', 2);
            return;
        }

        this.props.createRoom(value, rst => {
            if (rst.code === 1) { 
                //服务器返回创建房间成功.
                this.props.setRoomInfo(rst.data);
                this.changeTab('yellowTab');
            } else {
                Toast.info(rst.msg, 2);
            }
        });
    }
    promptRoom() {

        Modal.prompt('创建房间', '输入房间名称', [
            { text: '取消' },
            { text: '提交', onPress: this.createRoom.bind(this) }
        ], 'default', this.props.userName + '的房间');

    }
    removeRoom() {
        console.log(this.props);
        Modal.alert('删除房间', '确定删除房间?', [
            { text: '取消' },
            {
                text: '确定', onPress: () => {
                    let roomKey = this.props.drawInfo.key;
                    this.props.removeRoom(roomKey, rst => {
                        if (rst.code !== 1) {
                            Toast.fail(rst.msg, 1);
                        } else {
                            this.props.setRoomInfo(null);
                            this.changeTab('blueTab');
                        }
                    });
                }
            },
        ]);


    }

    renderContent(pageText) {
        let link, header;


        if (pageText === 'RoomList') {
            header = (<NavBar
                mode="dark"
                icon={<Icon type="left" />}
                leftContent='退出'
                onLeftClick={this.logout}
                rightContent={<span onClick={this.promptRoom}>创建房间  </span>}
            >房间列表
            </NavBar>);
            link = (
                <RoomList
                    switchSelectTab={() => {
                        this.changeTab(this.state.tabList[1].key);
                    }}
                ></RoomList>
            )

        } else if (pageText === 'DrawRoom') {
            var delRoom = this.props.drawInfo != null ? (
                <span onClick={this.removeRoom}>删除房间 </span>
            ) : null;

            let leftContent = this.props.drawInfo != null ? <Icon type="left" /> : null;

            header = (<NavBar
                icon={leftContent}
                mode="dark"
                onLeftClick={this.leaveRoom}
                rightContent={delRoom}
            >房间详情
            </NavBar>);
            link = <DrawRoom></DrawRoom>;
        }

        var body = (
            <div style={{ height: '100%' }}>

                {header}

                {link}
            </div>
        );
        return body;
    }

    changeTab(key) {
        this.setState({
            selectedTab: key
        });
    }

    render() {

        const footer = (
            <Footer
                list={this.state.tabList}
                selectedTab={this.state.selectedTab}
                renderContent={this.renderContent}
                switchSelectTab={this.changeTab}
            />
        )
        return footer;

    }
}

export default WelcomeDraw;