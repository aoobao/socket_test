import React from 'react';
import { connect } from 'react-redux';
import { addDrawInfo, pushDrawInfo } from '@/reducers/modules/drawInfo';

import CanvasBoard from '../components/CanvasBoard';
import MissDrawRoom from '../components/MissDrawRoom';

import '../css/canvas.scss';

@connect(state => { return { drawInfo: state.drawInfo.drawInfo }; }, {
  addDrawInfo, pushDrawInfo
})
class DrawRoom extends React.Component {
  render() {
    console.log('DrawRoom详情渲染');
    var body;
    if (this.props.drawInfo == null) {
      body = <MissDrawRoom title="你还没有进入房间" />
    } else {
      body = (
        <CanvasBoard
          drawInfo={this.props.drawInfo}
          addDrawInfo={this.props.addDrawInfo}
          pushDrawInfo={this.props.pushDrawInfo}
        />
      )
    }
    return (
      <div className="DrawRoom">
        {body}
      </div>
    );
  }
}

export default DrawRoom;
