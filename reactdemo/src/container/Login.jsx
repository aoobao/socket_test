import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { login, setUserName } from '../reducers/modules/personInfo';
import { Toast } from 'antd-mobile';

// import api from '../api/socket';

import '../css/login.css';

@connect(state => state.personInfo
  , { login, setUserName })
class Login extends Component {
  
  login(e) {
    // console.log(e.keyCode);
    if (e.keyCode === 13) {
      console.log('登录..');
      e.preventDefault();
      let name = e.target.value;
      if (name.length > 0) {
        this.props.login(name)

      } else {
        Toast.fail('请输入名字', 1);
      }
    }
  }
  // logintest() {
  //   let name = 'test';
  //   api.login(name, rst => {
  //     alert('返回');
  //   });
  // }
  render() {
    console.log('登录页面渲染');
    return (
      <div className="login page">
        {this.props.userName ? <Redirect to='/WelcomeDraw' /> : null}
        <div className="form">
          <h3 className="title">请输入你的名字</h3>
          <input className="usernameInput" type="text" maxLength="8" onKeyUp={this.login.bind(this)} />
          {/* <button onClick={this.logintest.bind(this)}>test</button> */}
        </div>
      </div>
    );
  }
}

export default Login;
