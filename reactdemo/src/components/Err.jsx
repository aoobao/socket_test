import React, { Component } from 'react';

class Err extends Component {
  render() {
    return (
      <div className="Err">
        <h1>你访问的路径不存在</h1>
      </div>
    );
  }
}

export default Err;
