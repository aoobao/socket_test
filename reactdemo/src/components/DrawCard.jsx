import React from 'react';
import { Card, WingBlank, WhiteSpace } from 'antd-mobile';

class DrawCard extends React.Component {

    remarkText() {
        let strText = '';
        let item = this.props.item;
        if (item.personList.length === 0) {
            strText = "还没有人在房间里画画...";
        } else if (item.personList.length <= 3) {
            for (const person of item.personList) {
                strText += "," + person.userName;
            }
            strText += " 正在房间里画画...";
            strText = strText.substr(1);
        } else {
            for (let i = 0; i < 3; i++) {
                const person = item.personList[i];
                strText += "," + person.userName;
            }
            strText += " 等人正在房间里画画...";
            strText = strText.substr(1);
        }
        return strText;
    }

    render() {
        var item = this.props.item;
        return (
            <div key={this.props.key} onClick={() => { this.props.enterRoom(item.key) }}>
                <WingBlank size="lg">
                    <WhiteSpace size="lg" />
                    <Card>
                        <Card.Header
                            title={item.roomName}
                            //thumb="https://cloud.githubusercontent.com/assets/1698185/18039916/f025c090-6dd9-11e6-9d86-a4d48a1bf049.png"
                            extra={<span>{item.createUserName}</span>}
                        />
                        <Card.Body>
                            <div>{this.remarkText()}</div>
                        </Card.Body>
                        <WhiteSpace size="lg" />
                    </Card>
                    <WhiteSpace size="lg" />
                </WingBlank>
            </div>
        );
    }
}


export default DrawCard;