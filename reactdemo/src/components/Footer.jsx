import React from 'react';

import { TabBar } from 'antd-mobile';

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.createTabBarItem = this.createTabBarItem.bind(this);
    }
    createTabBarItem(item, selectedTab) {
        var tItem = (
            <TabBar.Item
                title={item.title}
                key={item.key}
                icon={
                    <div style={{
                        width: '22px',
                        height: '22px',
                        background: `url(${item.iconUrl}) center center /  21px 21px no-repeat`
                    }}
                    />
                }
                selectedIcon={<div style={{
                    width: '22px',
                    height: '22px',
                    background: `url(${item.selectedIconUrl}) center center /  21px 21px no-repeat`
                }}
                />}
                selected={selectedTab === item.key}
                onPress={() => {
                    this.props.switchSelectTab(item.key);
                }}
            >
                {this.props.renderContent(item.body)}
            </TabBar.Item>
        );
        return tItem;
    }
    render() {
        let list = this.props.list.map(t => {
            return this.createTabBarItem(t, this.props.selectedTab);
        });

        let app = (
            <div style={{ position: 'fixed', height: '100%', width: '100%', top: 0 }}>
                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#33A3F4"
                    barTintColor="white"
                >
                    {list}
                </TabBar>
            </div>
        );
        return app;
    }
}

export default Footer;