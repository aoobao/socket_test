import React from 'react';

class MissDrawRoom extends React.Component {
    render() {
        return (
            <div className="Err">
                <h1>{this.props.title}</h1>
            </div>
        )
    }
}

export default MissDrawRoom;