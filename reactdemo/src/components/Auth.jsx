import React from 'react';
import { withRouter,Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

@withRouter
@connect(state => state.personInfo, {})
class Auth extends React.Component {
    render() {
        const login = { '/login': 1, '/': 2 };
        if(this.props.userName == null){
            if(!(this.props.location.pathname in login)){
                console.log('未登录');
                return <Redirect to='/' />
            }
        }
        return null;
    }
}

export default Auth;