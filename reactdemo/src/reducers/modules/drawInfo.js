import api from '../../api/socket';
//import { stat } from 'fs';

export const SET_ROOM_LIST = 'set room list';
export const SET_ROOM_INFO = 'set room info';
export const CREATE_ROOM = 'create room';
export const REMOVE_ROOM = 'remove room';
const CHANGE_SELECTED_TAB = 'change select tab';
const ADD_DRAW_INFO = 'add draw info';
const LEAVE_ROOM = 'leave room';
const CHANGE_ROOM = 'change room';

const _state = {
    drawList: [],
    drawInfo: null,
    selectedTab: 'blueTab'
}
/*
{
    roomName: '新的房间',
    createUserKey: 'admin',
    createUserName: 'admin',
    key: 0,
    personList: [{
        userName: 'aoobao',
        userKey: '1'
    }, {
        userName: 'wa',
        userKey: '4'
    }],
    createTime: new Date(),
    drawInfoList: []
}
*/
export function drawInfo(state = _state, action) {
    // console.log(state);
    switch (action.type) {
        case SET_ROOM_LIST:
            return {
                ...state,
                drawList: action.drawList
            }
        case SET_ROOM_INFO:
            return {
                ...state,
                drawInfo: action.drawInfo,
                //selectedTab: 'yellowTab'
            }
        case CREATE_ROOM:
            state.drawList.unshift(action.room);
            return {
                ...state
            }
        case LEAVE_ROOM:
            let roomList = state.drawList.map(t => {
                if (t.key === action.roomInfo.key) {
                    return action.roomInfo
                } else {
                    return t;
                }
            })
            return {
                drawList: roomList,
                drawInfo: null,
                selectedTab: 'blueTab'
            }
        case REMOVE_ROOM:
            for (let i = 0; i < state.drawList.length; i++) {
                const draw = state.drawList[i];
                if (draw.key === action.key) {
                    state.drawList.splice(i, 1);
                    break;
                }
            }
            return {
                ...state
            }
        case CHANGE_ROOM:
            var list = state.drawList.map(t => {
                if (t.key === action.room.key) {
                    return action.room;
                } else {
                    return t;
                }
            });
            return {
                ...state,
                drawList: list
            }
        case CHANGE_SELECTED_TAB:
            return {
                ...state,
                selectedTab: action.selectedTab
            }

        case ADD_DRAW_INFO:
            var obj = {
                ...state
            }
            obj.drawInfo.drawInfoList.push(action.drawInfo);
            return obj;
        default:
            //console.log('未知动作:' + action.type);
            break;
    }
    return {
        ...state
    };
}

export function switchSelectTab(tabName) {
    return {
        type: CHANGE_SELECTED_TAB,
        selectedTab: tabName
    }
}

/**
 * 获取房间列表
 * @param {Function} fn 
 */
export function getRoomList(fn) {
    return dispath => {
        api.getRoomList(rst => {
            if (rst.code === 1) {
                dispath({
                    type: SET_ROOM_LIST,
                    drawList: rst.data
                })
            }
            if (typeof fn === 'function') fn(rst);
        });
    }
}

/**
 * 创建新房间
 * @param {String} roomName 
 * @param {Function} fn 
 */
export function createRoom(roomName, fn) {
    if (!roomName) return;
    console.log('创建房间请求');
    return dispath => {
        api.addRoom(roomName, rst => {
            //console.log(rst);
            // if (rst.code === 1) {
            //     dispath({
            //         type: CREATE_ROOM,
            //         room: rst.data
            //     });
            // }
            if (typeof fn === 'function') fn(rst);
        });
    }
}

export function addRoom(room) {
    return {
        type: CREATE_ROOM,
        room: room
    }
}

/**
 * 获取房间详情.
 * @param {String} roomKey 
 * @param {Function} fn 
 */
export function getRoomInfo(roomKey, fn) {
    if (!roomKey) return;
    console.log('获取房间详情,进入房间');
    return dispath => {
        api.getRoomInfo(roomKey, rst => {
            if (rst.code === 1) {
                dispath({
                    type: SET_ROOM_INFO,
                    drawInfo: rst.data
                })
            }
            if (typeof fn === 'function') fn(rst);
        });
    }
}

export function setRoomInfo(drawInfo) {
    return {
        type: SET_ROOM_INFO,
        drawInfo: drawInfo
    }
}
/**
 * 移除房间.
 * @param {String} roomKey 
 * @param {Function} fn 
 */
export function removeRoom(roomKey, fn) {
    if (!roomKey) return;
    console.log('移除房间');
    return dispath => {
        api.removeRoom(roomKey, rst => {
            // if (rst.code === 1) {
            //     dispath({
            //         type: REMOVE_ROOM,
            //         key: roomKey
            //     })
            // }
            if (typeof fn === 'function') fn(rst);
        });
    }
}


export function subRoom(roomKey) {
    return {
        type: REMOVE_ROOM,
        key: roomKey
    }
}

export function leaveRoom(fn) {
    return dispath => {
        api.leaveRoom(rst => {
            if (rst.code === 1) {
                dispath({
                    type: LEAVE_ROOM,
                    roomInfo: rst.data
                })
            }
            if (typeof fn === 'function') fn(rst);
        });
    }
}

export function changeRoom(room) {
    return {
        type: CHANGE_ROOM,
        room: room
    }
}

/**
 * 添加线条.
 * @param {Object} drawInfo 
 */
export function addDrawInfo(drawInfo) {
    return dispath => {
        api.addDrawInfo(drawInfo, rst => {
            if (rst.code === 1) {
                dispath({
                    drawInfo,
                    type: ADD_DRAW_INFO
                })
            }
        });
    }
}

export function pushDrawInfo(drawInfo) {
    return {
        drawInfo,
        type: ADD_DRAW_INFO
    }
}