import {combineReducers } from 'redux';
import {personInfo} from './modules/personInfo';
import {drawInfo} from './modules/drawInfo';

export default combineReducers({
    drawInfo,    
    personInfo
});