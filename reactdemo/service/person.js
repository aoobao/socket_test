// var roomList = require('./roomList');

var userCount = 1;
var connectCount = 1;
var users = {
    admin: {
        key: 'admin',
        userName: 'admin',
        createTime: new Date(),
        roomKey: null
    }
};

var person = {
    userCount() { return userCount },
    connectCount() { return connectCount },
    users() {
        return users;
    },
    addUser(key) {
        if (key in users) return false;
        users[key] = {
            key,
            userName: null,
            createTime: new Date(),
            roomKey: null
        }
        connectCount++;
        return true;
    },
    deleteUser(key) {
        if (!(key in this.users)) return false;
        this.changeName(key, null);
        connectCount--;
        delete users[key];
        console.log(Object.keys(users));
        return true;
    },
    changeName(key, name) {
        if (!(key in users)) return false;
        if (name) {
            if (!users[key].userName) userCount++;
        } else {
            if (users[key].userName) userCount--;
        }
        users[key].userName = name;
    },
    getUserByKey(key) {
        return users[key];
    }
};

module.exports = person;