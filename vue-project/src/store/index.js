import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

var state = {
    todolist: [],
    count: 0
}

var modules = Object.assign({}, state);

const store = new Vuex.Store({
    state: modules,
    mutations: {
        addTodo(state,val) {
            state.todolist.push({
                title: val,
                id: ++ids
            });
        },
        removeTodo(id) {
            for (let i = 0; i < this.state.todolist.length; i++) {
                const item = this.state.todolist[i];
                if (item.id == id) {
                    this.state.todolist.splice(i, 1);
                    return true;
                }
            }
            return false;
        }
    }
});

export default store;