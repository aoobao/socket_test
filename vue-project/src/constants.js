import Vue from 'vue'


export default {
    ADD_TODO: "ADD_TODO",
    TOGGLE_TODO: "TOGGLE_TODO",
    TOGGLE_ALL_TODO: "TOGGLE_ALL_TODO",
    DESTORY_TODO: "DESTORY_TODO",
    CLEAR_TODOS: "CLEAR_TODOS",
    Event: new Vue()
}