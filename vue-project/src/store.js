let ids = 0;

export default {
    state: {
        todolist: []
    },
    addTodo(val) {
        this.state.todolist.push({
            title: val,
            id: ++ids
        });
    },
    removeTodo(id) {
        for (let i = 0; i < this.state.todolist.length; i++) {
            const item = this.state.todolist[i];
            if (item.id == id) {
                this.state.todolist.splice(i, 1);
                return true;
            }
        }
        return false;
    }
}