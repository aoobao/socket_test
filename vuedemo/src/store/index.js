import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import personInfo from './modules/personInfo';
import drawInfo from './modules/drawInfo';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
    modules: {
        personInfo,
        drawInfo
    },
    strict: debug,
    plugins: debug ? [createLogger()] : [],
});

export default store;
