import api from '@/api/socket';
const state = {
    drawList: [],
    drawInfo: {
        roomName: '新的房间',
        createUserKey: 'admin',
        createUserName: 'admin',
        key: 0,
        personList: [{ userName: 'aoobao', userKey: '1' }, { userName: 'wa', userKey: '4' }],
        createTime: new Date(),
        drawInfoList: []
    }
};

const getters = {
    drawList: state => state.drawList,
    drawInfo: state => state.drawInfo
};

const mutations = {
    //添加房间
    addRooms(state, draw) {
        state.drawList.unshift(...draw);
    },
    //移除房间
    removeRoom(state, key) {
        for (let i = 0; i < state.drawList.length; i++) {
            const draw = state.drawList[i];
            if (draw.key == key) {
                state.drawList.splice(i, 1);
                return true;
            }
        }
        return false;
    },
    setRoomInfo(state, info) {
        state.drawInfo = info;
    },
    clearRooms() {
        state.drawList.splice(0);
    },
    addDrawInfo(state, drawInfo) {
        state.drawInfo.drawInfoList.push(drawInfo);
    }
};

const actions = {
    //获取房间列表.
    getRoomList(context) {
        return new Promise((resolve, reject) => {
            api.getRoomList(rst => {
                //this.setRoomList(context, rst.data);
                if (rst.code)
                    context.dispatch('setRoomList', rst.data);

                resolve(rst);
            });
        });

    },
    setRoomList(context, data) {

        context.commit('clearRooms');
        context.commit('addRooms', data);
    },
    setRoomInfo(context, info) {
        context.commit('setRoomInfo', info);
    },
    createRoom(context, data) {
        return new Promise((resolve, reject) => {
            api.addRoom(data.roomName, rst => {
                if (rst.code) {
                    context.commit('addRooms', rst.data);
                    context.commit('setRoomInfo', rst.data);
                }
                resolve(rst);
            });
        });

    },
    addRoom(context, data) {
        context.commit('addRooms', data);
        context.commit('setRoomInfo', data);

        console.log('添加房间', state.drawList);

    },
    removeRoom(context, key) {
        return new Promise((resolve, reject) => {
            api.removeRoom(key, rst => {
                if (rst.code) context.commit('removeRoom', key);
                resolve(rst);
            });
        });
    },
    getRoomInfo(context, key) {
        return new Promise((resolve, reject) => {
            api.getRoomInfo(key, rst => {
                console.log(rst.data);
                if (rst.code) context.commit('setRoomInfo', rst.data);
                resolve(rst);
            });
        });
    },
    addDrawInfo(context, drawInfo) {
        return new Promise((resolve, reject) => {
            api.addDrawInfo(drawInfo, rst => {
                context.commit('addDrawInfo', drawInfo);
                resolve(rst);
            });
        });
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}