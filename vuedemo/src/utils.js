window.Utils = {
    alert: function (msg, title, callback) {
        alert(msg);
        if (typeof title == 'function') callback = title;
        if (typeof callback == 'function') callback();
    },
    getItem: function (value) {
        if (window.localStorage) {
            return window.localStorage.getItem(value);
        } else {
            return this.getCookie(value);
        }
    },
    //设置 值
    setItem: function (name, value) {
        if (window.localStorage) {
            return window.localStorage.setItem(name, value);
        } else {
            return this.setCookie(name, value);
        }

    },
    getCookie: function (keyName) {
        var cookieTrim = this.trim(document.cookie, "g");
        var arr, reg = new RegExp("(^|;)" + keyName + "=([^;]*)(;|$)");
        if (arr = cookieTrim.match(reg))
            return unescape(arr[2]);
        else
            return null;
    },

    //设置Cookie
    setCookie: function (keyName, keyValue, expireMins = 1440) {
        var date = new Date();
        date.setTime(date.getTime() + expireMins * 60 * 1000);
        document.cookie = keyName + " = " + escape(keyValue) + ((expireMins == null) ? "" : "; expire = " + date.toGMTString());
    },
}