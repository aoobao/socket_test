// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import api from "./api/socket";
import MSG from "./api/connectMsg";
import './utils';


Vue.config.productionTip = false

/* eslint-disable no-new */
var vm = new Vue({
  //el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});

//socket service push

socket.on("connect", () => {
  console.log("connect");
  store.commit("setKey", { key: socket.id });
  vm.$mount('#app');
});

socket.on("disconnect", () => {
  console.log("disconnect");
  store.commit('setUserName', { userName: null });
  router.replace('/');
});

// socket.on(MSG.USER_LOGIN, rst => {
//   console.log('service push user login');
//   store.commit('setUserName', { userName: rst.userName });
// });

// socket.on(MSG.USER_LOGOUT, () => {
//   store.commit('setUserName', { userName: null });
// });

//增加房间
socket.on(MSG.ADD_ROOM, rst => {
  console.log('读取到新增房间列表', rst);
  if (rst.code) {
    store.dispatch('addRoom', rst.data);
  }
});
//移除房间
socket.on(MSG.REMOVE_ROOM, rst => {
  console.log('读取到移除房间列表', rst);
  if (rst.code)
    store.dispatch('removeRoom', rst.data.key);
});
