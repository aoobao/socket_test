import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import WelcomeDraw from '@/components/WelcomeDraw'
import CreateRoom from '@/components/CreateRoom'
import DrawRoom from '@/components/DrawRoom';
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/WelcomeDraw',
      name: 'WelcomeDraw',
      component: WelcomeDraw,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/CreateRoom',
      name: 'CreateRoom',
      component: CreateRoom,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/DrawRoom',
      name: 'DrawRoom',
      component: DrawRoom,
      meta: {
        requiresAuth: true
      }
    }
  ]

});
