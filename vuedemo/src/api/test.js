function newGuid() {
    var guid = "";
    for (var i = 1; i <= 32; i++) {
        var n = Math.floor(Math.random() * 16.0).toString(16);
        guid += n;
        if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
            guid += "-";
    }
    return guid;
}

function success(data, msg = '成功', number = 1) {
    let obj = {
        code: number,
        msg
    };
    if (data != undefined) {
        obj.data = data;
    }
    return obj;
}

function err(data, msg = '失败', number = 0) {
    let obj = {
        code: number,
        msg
    };
    if (data != undefined) {
        obj.data = data;
    }
    return obj;
}

const arr = [{
    roomName: 'feng的房间1',
    createUserKey: 'admin',
    key: 1,
    imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
    personList: [],
    createTime: new Date()
}, {
    roomName: 'feng的房间2',
    createUserKey: 'admin',
    key: 2,
    imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
    personList: [{ userName: 'aoobao', userKey: '1' }, { userName: 'wa', userKey: '4' }],
    createTime: new Date()
}, {
    roomName: 'feng的房间3',
    createUserKey: 'admin',
    key: 3,
    imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
    personList: [{ userName: 'aoobao', userKey: '1' }, { userName: 'feng', userKey: '2' },
    { userName: 'cl', userKey: '3' }, { userName: 'wa', userKey: '4' }],
    createTime: new Date()
}];

var key = arr.length + 1;

export default {
    //连接服务器.
    connect(cb) {
        var key = newGuid();
        setTimeout(() => {
            cb(success({ key }));
        }, 100);
    },
    //登录用户名
    login(userName, cb) {
        setTimeout(() => {
            cb(success());
        }, 200);
    },
    //获取当然房间列表.
    getRoomList(cb) {
        setTimeout(() => {
            cb(success(arr))
        }, 500);
    },
    addRoom(opt, cb) {
        setTimeout(() => {
            var obj = {
                roomName: opt.roomName,
                createUserKey: opt.userKey,
                key: ++key,
                imageUrl: 'http://image.fengyitong.name/Blog%2F20170927%2F9885883_122140091001_2',
                personList: [],
                createTime: new Date()
            }
            arr.unshift(obj);
            cb(success(obj));
        }, 200);
    },
    removeRoom(key, cb) {
        setTimeout(() => {
            for (let i = 0; i < arr.length; i++) {
                const item = arr[i];
                if (item.key == key) {
                    arr.splice(i, 1);
                    cb(success(item));
                    return;
                }
            }
            cb(err('not found'));
        }, 200);
    }
}