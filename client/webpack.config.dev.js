const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    devtool: 'eval-source-map',
    devServer: {
        host: process.env.HOST,
        port: 8080,
        inline: true,
        //historyApiFallback: true,
        // overlay: {
        //     errors: true,
        //     warnings: true
        // }
        //hotOnly:true
    },
    entry: {
        index: './lib/index.js'
    },
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name]-[hash].js'
    },
    module: {
        rules: [

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.impl.html',
            inject: false
        }),

    ]
};