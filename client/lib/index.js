
import config from './config';
//import io from 'socket.io-client';    //也可以把socket.io和自己的代码打包在一起.

const socket = io(config.socketUrl);

socket.on('connect',()=>{
    console.log('connect ' + socket.id);

    appendText("连接成功 " + socket.id);
});

//追加文字到html页面上
function appendText(strText){
    var p = document.createElement("p");
    p.innerText = strText;

    document.getElementById("message").appendChild(p);
}