const HtmlWebpackPlugin = require('html-webpack-plugin');       //html模板插件
const CleanWebpackPlugin = require("clean-webpack-plugin");     //清除历史打包文件
const Uglify = require('uglifyjs-webpack-plugin');              //压缩js代码

const path = require('path');

module.exports = {
    entry: {
        index: './lib/index.js'
    },
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name]-[hash].js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.impl.html',
            inject: false
        }),
        new CleanWebpackPlugin('./dist/*.*', {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new Uglify(),        //压缩js代码
    ]
};